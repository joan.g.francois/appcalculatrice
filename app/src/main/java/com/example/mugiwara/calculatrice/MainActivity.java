package com.example.mugiwara.calculatrice;


import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;

public class MainActivity extends Activity implements View.OnClickListener {

    Button [] buttonsNumbers= new Button[10];
    Button buttonPlus;
    Button buttonMoins;
    Button buttonDiv;
    Button buttonMul;
    Button buttonC;
    Button buttonEgal;
    Button buttonPoint;
    TextView ecran;

    CalculateModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonPlus = (Button) findViewById(R.id.buttonPlus);
        buttonPlus.setOnClickListener(this);
        buttonMoins = (Button) findViewById(R.id.buttonMoins);
        buttonMoins.setOnClickListener(this);
        buttonDiv = (Button) findViewById(R.id.buttonDivision);
        buttonDiv.setOnClickListener(this);
        buttonMul = (Button) findViewById(R.id.buttonMultiplier);
        buttonMul.setOnClickListener(this);
        buttonPoint = (Button) findViewById(R.id.buttonPoint);
        buttonPoint.setOnClickListener(this);
        buttonC = (Button) findViewById(R.id.buttonC);
        buttonC.setOnClickListener(this);
        buttonEgal = (Button) findViewById(R.id.buttonEgal);
        buttonEgal.setOnClickListener(this);
        for(int i=0; i< buttonsNumbers.length; i++){
            buttonsNumbers[i] = (Button) findViewById(

                    getResources().getIdentifier("button"+i, "id", getPackageName())
            );
            buttonsNumbers[i].setOnClickListener(this);
        }

        ecran = (TextView) findViewById(R.id.EditText01);
        model = new CalculateModel();
    }

    @Override
    public void onClick(View v) {
        if(v.equals(buttonPlus)){
            model.setOperator('+');
        }else if(v.equals(buttonMoins)){
            model.setOperator('-');
        }else if(v.equals(buttonDiv)){
            model.setOperator('/');
        }else if(v.equals(buttonMul)){
            model.setOperator('*');
        }else if(v.equals(buttonC)){
            model.clear();
            ecran.setText("0");
        }else if(v.equals(buttonEgal)){
            double result = model.calculate();
            ecran.setText(String.valueOf(result));
            model.setFirstValue(String.valueOf(result));
            model.setSecondValue("");
            model.setOperator(' ');
        }else{
            Button numberButton = (Button) v;
            if(model.getOperator() == ' '){
                model.addFirstValueNumber(numberButton.getText().toString());
                ecran.setText(String.valueOf(model.getFirstValue()));
            }else{
                model.addSecondValueNumber(numberButton.getText().toString());
                ecran.setText(String.valueOf(model.getSecondValue()));
            }
        }
    }
}
