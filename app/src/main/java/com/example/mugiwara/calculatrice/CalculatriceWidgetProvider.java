package com.example.mugiwara.calculatrice;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

/**
 * Created by Mugiwara on 09/04/2017.
 */

public class CalculatriceWidgetProvider extends AppWidgetProvider {

    private static RemoteViews mRemoteViews;
    private static AppWidgetManager mWidgetManager;
    private static Intent mIntent;
    private static Context mContext;
    private static int[] mWidgetIds;

    private final static String ACTION_PLUS= "+";
    private final static String ACTION_MOINS= "-";
    private final static String ACTION_DIV= "/";
    private final static String ACTION_MUL= "*";
    private final static String ACTION_ClEAR= "C";
    private final static String ACTION_POINT= ".";
    private final static String ACTION_EGAL= "=";
    private final static String ACTION_0= "0";
    private final static String ACTION_1= "1";
    private final static String ACTION_2= "2";
    private final static String ACTION_3= "3";
    private final static String ACTION_4= "4";
    private final static String ACTION_5= "5";
    private final static String ACTION_6= "6";
    private final static String ACTION_7= "7";
    private final static String ACTION_8= "8";
    private final static String ACTION_9= "9";


    private static CalculateModel model;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        initVariables(context);

        mIntent = new Intent(context, CalculatriceWidgetProvider.class);

        setupIntent(ACTION_PLUS, R.id.buttonPlus);
        setupIntent(ACTION_MOINS, R.id.buttonMoins);
        setupIntent(ACTION_MUL, R.id.buttonMultiplier);
        setupIntent(ACTION_DIV, R.id.buttonDivision);
        setupIntent(ACTION_ClEAR, R.id.buttonC);
        setupIntent(ACTION_POINT, R.id.buttonPoint);
        setupIntent(ACTION_EGAL, R.id.buttonEgal);
        for(int i=0; i < 10 ; i++) {
            setupIntent(String.valueOf(i), context.getResources().getIdentifier("button" + i, "id", context.getPackageName()));
        }

        updateWidget();
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        switch (action) {
            case ACTION_PLUS:
            case ACTION_MOINS:
            case ACTION_MUL:
            case ACTION_DIV:
            case ACTION_ClEAR:
            case ACTION_POINT:
            case ACTION_EGAL:
            case ACTION_0:
            case ACTION_1:
            case ACTION_2:
            case ACTION_3:
            case ACTION_4:
            case ACTION_5:
            case ACTION_6:
            case ACTION_7:
            case ACTION_8:
            case ACTION_9:
                myAction(action, context);
                break;
            default:
                super.onReceive(context, intent);
        }
    }

    private void myAction(String action, Context context) {
        if (model == null || mRemoteViews == null || mWidgetManager == null || mContext == null) {
            initVariables(context);
        }
        switch (action){
            case ACTION_PLUS:
            case ACTION_MOINS:
            case ACTION_MUL:
            case ACTION_DIV:
                model.setOperator(action.charAt(0));
                break;
            case ACTION_ClEAR:
                model.clear();
                mRemoteViews.setTextViewText(R.id.EditText01, "0");
                updateWidget();
                break;
            case ACTION_EGAL:
                double result = model.calculate();
                mRemoteViews.setTextViewText(R.id.EditText01, String.valueOf(result));
                model.clear();
                model.setFirstValue(String.valueOf(result));
                updateWidget();
                break;
            default:
                if(model.getOperator() == ' '){
                    model.addFirstValueNumber(action);
                    mRemoteViews.setTextViewText(R.id.EditText01, model.getFirstValue());
                }else{
                    model.addSecondValueNumber(action);
                    mRemoteViews.setTextViewText(R.id.EditText01, model.getSecondValue());
                }
                updateWidget();
                break;
        }
    }

    private void setupIntent(String action, int id) {
        mIntent.setAction(action);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 0, mIntent, 0);
        mRemoteViews.setOnClickPendingIntent(id, pendingIntent);
    }

    private void initVariables(Context context){
        mContext = context;
        updateWidgetIds();
        mRemoteViews = new RemoteViews(mContext.getPackageName(), R.layout.widget_layout);
        mWidgetManager = AppWidgetManager.getInstance(mContext);
        model = new CalculateModel();
    }


    private void updateWidget() {
        for (int widgetId : mWidgetIds) {
            mWidgetManager.updateAppWidget(widgetId, mRemoteViews);
        }
    }

    private void updateWidgetIds() {
        final ComponentName component = new ComponentName(mContext, CalculatriceWidgetProvider.class);
        mWidgetManager = AppWidgetManager.getInstance(mContext);
        mWidgetIds = mWidgetManager.getAppWidgetIds(component);
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
        if (mContext != null)
            updateWidgetIds();
    }
}
