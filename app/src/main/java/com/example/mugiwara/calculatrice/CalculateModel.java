package com.example.mugiwara.calculatrice;

/**
 * Created by Mugiwara on 09/04/2017.
 */

public class CalculateModel {
    private String firstValue = "";
    private String secondValue = "";
    private char operator=' ';

    public String getFirstValue() {
        return firstValue;
    }

    public void setFirstValue(String firstValue) {
        this.firstValue = firstValue;
    }

    public String getSecondValue() {
        return secondValue;
    }

    public void setSecondValue(String secondValue) {
        this.secondValue = secondValue;
    }

    public char getOperator() {
        return operator;
    }

    public void setOperator(char operator) {
        this.operator = operator;
    }

    public void addFirstValueNumber(String number){
        this.firstValue= this.firstValue + number;
    }

    public  void addSecondValueNumber(String number){
        this.secondValue= this.secondValue+ number;
    }

    public  double calculate(){
        double result= 0;
        switch (this.operator){
            case '+':
                result= Double.parseDouble(this.firstValue) + Double.parseDouble(this.secondValue);
                break;
            case '-':
                result= Double.parseDouble(this.firstValue) - Double.parseDouble(this.secondValue);
                break;
            case '*':
                result= Double.parseDouble(this.firstValue) * Double.parseDouble(this.secondValue);
                break;
            case '/':
                result= Double.parseDouble(this.firstValue) / Double.parseDouble(this.secondValue);
                break;
        }
        return result;
    }

    public void clear(){
        this.firstValue = "";
        this.secondValue = "";
        this.operator = ' ';
    }
}
